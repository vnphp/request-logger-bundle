<?php


namespace Vnphp\RequestLoggerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vnphp\RequestLoggerBundle\Entity\RequestLog;

class CleanupCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('request-logger:cleanup')
            ->addArgument('period', InputArgument::OPTIONAL, 'Period to cleanup', '90 days');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime("-{$input->getArgument('period')}");

        $repository = $this->getContainer()
            ->get('doctrine')
            ->getRepository(RequestLog::class);

        $deleted = $repository->deleteAllUntil($date);
        $output->writeln("Removed {$deleted} entity(ies).");
    }
}
