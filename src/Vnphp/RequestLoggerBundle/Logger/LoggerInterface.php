<?php

namespace Vnphp\RequestLoggerBundle\Logger;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface LoggerInterface
{
    public function log(Request $request, Response $response = null, $user = null);
}
