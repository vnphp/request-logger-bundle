<?php


namespace Vnphp\RequestLoggerBundle\Logger;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Vnphp\RequestLoggerBundle\Entity\RequestLog;

class EntityLogger implements LoggerInterface
{
    /**
     * @var Registry
     */
    protected $doctrine;

    /**
     * EntityLogger constructor.
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function log(Request $request, Response $response = null, $user = null)
    {
        $log = new RequestLog();
        $log->setByRequest($request);

        if ($response) {
            $log->setByResponse($response);
        }

        if ($user) {
            $log->setUserId($user->getId());
        }
        $manager = $this->doctrine->getManagerForClass(get_class($log));
        $manager->persist($log);
        $manager->flush();
    }
}
