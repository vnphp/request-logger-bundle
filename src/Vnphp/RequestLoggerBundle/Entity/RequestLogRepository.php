<?php


namespace Vnphp\RequestLoggerBundle\Entity;

use Doctrine\ORM\EntityRepository;

class RequestLogRepository extends EntityRepository
{
    public function deleteAllUntil(\DateTime $dateTime)
    {
        return $this->createQueryBuilder('log')
            ->andWhere('log.createdAt <= :deadline')
            ->setParameter('deadline', $dateTime)
            ->delete()
            ->getQuery()
            ->execute();
    }
}
