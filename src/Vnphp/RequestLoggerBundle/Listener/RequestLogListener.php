<?php


namespace Vnphp\RequestLoggerBundle\Listener;

use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Vnphp\RequestLoggerBundle\Logger\LoggerInterface;

class RequestLogListener
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var SecurityContextInterface
     */
    protected $securityContext;

    /**
     * RequestLogListener constructor.
     * @param LoggerInterface $logger
     * @param SecurityContextInterface $securityContext
     */
    public function __construct(LoggerInterface $logger, SecurityContextInterface $securityContext)
    {
        $this->logger = $logger;
        $this->securityContext = $securityContext;
    }

    public function onKernelTerminate(PostResponseEvent $event)
    {
        $token = $this->securityContext->getToken();
        if (!$token) {
            return;
        }

        $user = $token->getUser();
        if (!is_object($user)) {
            return;
        }

        $this->logger->log($event->getRequest(), $event->getResponse(), $user);
    }
}
