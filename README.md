# Request Logger bundle


[![build status](https://gitlab.com/vnphp/request-logger-bundle/badges/master/build.svg)](https://gitlab.com/vnphp/request-logger-bundle/commits/master)
[![code quality](https://insight.sensiolabs.com/projects/65703076-89c1-40e2-aafa-8d8e9692fe0e/big.png)](https://insight.sensiolabs.com/projects/65703076-89c1-40e2-aafa-8d8e9692fe0e)


## Installation 

```
composer require vnphp/request-logger-bundle
```

You need to have `git` installed. 

## Usage

Add the bundle to your `AppKernel.php`:

```php
<?php 

$bundles = array(          
            new Vnphp\RequestLoggerBundle\VnphpRequestLoggerBundle(),
        );

```
